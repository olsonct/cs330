#!/usr/bin/env ruby

raw = IO.read(ARGV[0])
numbChar = ARGV[1]

puts "RAW:\n" + raw + "\n"
cooked = raw.gsub(/^.{1, #{numbChar}}/, "DEEZ")

puts "COOKED:\n" + cooked
